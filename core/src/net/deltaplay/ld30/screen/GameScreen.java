package net.deltaplay.ld30.screen;

import java.util.Iterator;

import net.deltaplay.ld30.Assets;
import net.deltaplay.ld30.Ld30;
import net.deltaplay.ld30.model.Ball;
import net.deltaplay.ld30.model.Direction;
import net.deltaplay.ld30.model.Entity;
import net.deltaplay.ld30.model.FlyingSaw;
import net.deltaplay.ld30.model.Spike;
import net.deltaplay.ld30.model.Water;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class GameScreen extends ScreenAdapter {
    private static final float UPDATES_PER_SECOND = 50f;
    public static final float TILE_WIDTH = 32f;
    public static final int DEF_LIVES = 3;

    public enum GameState {
        RUNNING, PAUSED, FINISHED
    }

    Ld30 game;
    TiledMap map;
    OrthographicCamera camera;
    OrthogonalTiledMapRenderer renderer;
    SpriteBatch batch;
    ShapeRenderer shapeRenderer = new ShapeRenderer();

    private Ball ball;
    private int lives;

    private GameState state;

    private int currentWorld;

    private float accum;

    private Array<Entity> entities;
    private Array<Ball> friends;

    private float mapWidth;
    private float mapHeight;

    private String skyColor;

    private Stage guiStage;
    private Table lifeBar;
    private Table healthBar;

    private Table infoTable;

    private Image next, back;

    public GameScreen(Ld30 game) {
        this.game = game;

        this.state = GameState.PAUSED;

        this.camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());

        this.batch = game.getBatch();

        Assets.loadAll();

        this.lives = DEF_LIVES;

        this.guiStage = new Stage(new ExtendViewport(
                (float) Gdx.graphics.getWidth(),
                (float) Gdx.graphics.getHeight()));

        startWorld(1, true);

        Gdx.input.setInputProcessor(new InputMultiplexer(new InputHandler()));
    }

    public void startWorld(int number, boolean newGame) {
        float unitScale = 1;
        this.map = new TmxMapLoader().load("data/level" + number + ".tmx");
        this.renderer = new OrthogonalTiledMapRenderer(map, unitScale);

        mapWidth = (Integer) map.getProperties().get("width") * TILE_WIDTH;
        mapHeight = (Integer) map.getProperties().get("height") * TILE_WIDTH;
        skyColor = (String) map.getProperties().get("SKY");

        // Models
        MapObject start = map.getLayers().get("OBJECTS").getObjects()
                .get("BALL");

        float bx = (Float) start.getProperties().get("x");
        float by = (Float) start.getProperties().get("y");

        if (newGame) {
            ball = new Ball(new Vector2(bx, by), true);
            this.lives = DEF_LIVES;
        } else {

            if (ball.getHealth() <= 0) {
                ball = new Ball(new Vector2(bx, by), true);
            } else {
                ball.setPosition(new Vector2(bx, by));
            }
        }

        entities = new Array<Entity>();
        friends = new Array<Ball>();
        populateObjects();

        this.state = GameState.RUNNING;
        this.currentWorld = number;

        guiStage.clear();

        if (currentWorld == 4) {
            displayInfo(Assets.youwon, "Press SPACE to play again");
            Assets.loop.play();
        } else {
            Assets.loop.stop();

            healthBar = new Table(Assets.skin);
            updateHealthBar();

            lifeBar = new Table(Assets.skin);
            updateLifeBar();

            addIndicators();
        }
    }

    private void populateObjects() {
        MapObjects objects = map.getLayers().get("OBJECTS").getObjects();

        Iterator<MapObject> iter = objects.iterator();

        while (iter.hasNext()) {
            MapObject obj = iter.next();
            float x = (Float) obj.getProperties().get("x");
            float y = (Float) obj.getProperties().get("y");
            if ("SPIKE".equals(obj.getProperties().get("type"))) {
                Spike s = new Spike(false, new Vector2(x, y));
                entities.add(s);
            } else if ("TOP_SPIKE".equals(obj.getProperties().get("type"))) {
                Spike s = new Spike(true, new Vector2(x, y));
                entities.add(s);
            } else if ("SAW".equals(obj.getProperties().get("type"))) {
                FlyingSaw s = new FlyingSaw(new Vector2(x, y));
                entities.add(s);
            } else if ("WATER".equals(obj.getProperties().get("type"))) {
                Water w = new Water(new Vector2(x, y));
                entities.add(w);
            } else if ("BALL".equals(obj.getProperties().get("type"))) {
                Ball b = new Ball(new Vector2(x, y), false);
                friends.add(b);
            }
        }

    }

    @Override
    public void render(float delta) {
        switch (state) {
        case RUNNING:
            renderRunning(delta);
            break;

        case FINISHED:
            renderGame(delta);
            break;

        case PAUSED:
            renderGame(delta);
            break;

        default:
            break;
        }

        guiStage.act();
        guiStage.draw();
    }

    private void renderRunning(float delta) {
        accum += delta;

        while (accum > 1.0f / UPDATES_PER_SECOND) {
            update(1.0f / UPDATES_PER_SECOND);
            accum -= (1.0f / UPDATES_PER_SECOND);
        }

        renderGame(delta);
    }

    private void renderGame(float delta) {
        centerCamera();
        camera.update();
        renderer.setView(camera);

        if ("DARK".equalsIgnoreCase(skyColor))
            Gdx.gl.glClearColor(0.173f, 0.353f, 0.627f, 1);
        else
            Gdx.gl.glClearColor(0, 0.682f, 0.886f, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        TextureRegion ballRegion;

        if (ball.isDead()) {
            ballRegion = Assets.ballDead;
        } else if (ball.isInjured()) {
            ballRegion = Assets.ballSad;
        } else {
            if (Gdx.input.isKeyPressed(Keys.UP))
                ballRegion = Assets.ballUp;
            else if (Gdx.input.isKeyPressed(Keys.DOWN))
                ballRegion = Assets.ballDown;
            else if (Gdx.input.isKeyPressed(Keys.RIGHT))
                ballRegion = Assets.ballRight;
            else if (Gdx.input.isKeyPressed(Keys.LEFT))
                ballRegion = Assets.ballLeft;
            else
                ballRegion = Assets.ball;
        }
        batch.draw(ballRegion, ball.getPosition().x, ball.getPosition().y);

        renderObjects();
        batch.end();

        renderer.render();
    }

    private void renderObjects() {

        for (Entity ent : entities) {
            float x = ent.getPosition().x;
            float y = ent.getPosition().y;
            if (ent instanceof Spike) {
                Spike s = (Spike) ent;
                if (s.isTop())
                    batch.draw(Assets.topSpikes, x, y);
                else
                    batch.draw(Assets.spikes, x, y);
            } else if (ent instanceof FlyingSaw) {
                batch.draw(Assets.saw, x, y);
            }
        }

        for (Ball ball : friends) {
            batch.draw(Assets.ball2, ball.getPosition().x, ball.getPosition().y);
        }
    }

    private void update(float delta) {
        updateBall(delta, ball);
        updateFriends(delta);
        updateEntities(delta);
    }

    private void updateFriends(float delta) {
        for (Ball friend : friends) {
            updateBall(delta, friend);
        }
    }

    private void updateEntities(float delta) {
        for (Entity ent : entities) {
            ent.update(delta);

            if (ent instanceof FlyingSaw) {
                FlyingSaw f = (FlyingSaw) ent;

                MapObjects objects = map.getLayers().get("COLLISION")
                        .getObjects();

                for (RectangleMapObject polyObject : objects
                        .getByType(RectangleMapObject.class)) {
                    Rectangle rect = polyObject.getRectangle();

                    if (Intersector.overlaps(f.getBounds(), rect)) {
                        f.changeMovingDirection();
                    }
                }

            }
        }
    }

    private void updateBall(float delta, Ball theBall) {
        Vector2 tmp = theBall.getPosition().cpy();
        theBall.update(delta);

        float mapWidth = (Integer) map.getProperties().get("width")
                * TILE_WIDTH;

        Direction collidedY = Direction.NONE;
        Direction collidedX = Direction.NONE;

        MapObjects objects = map.getLayers().get("COLLISION").getObjects();
        for (RectangleMapObject polyObject : objects
                .getByType(RectangleMapObject.class)) {
            Rectangle rect = polyObject.getRectangle();

            if (Intersector.overlaps(theBall.getBounds(), rect)) {
                if (rect.getY() > tmp.y) {
                    // Top collision
                    collidedY = Direction.UP;
                } else {
                    if (rect.getX() > theBall.getBounds().x) {
                        // Right side collision
                        collidedX = Direction.RIGHT;
                    } else if (rect.getX() + rect.getWidth() < theBall
                            .getBounds().x) {
                        // Left side collision
                        collidedX = Direction.LEFT;
                    }

                    if (rect.getY() + rect.height < theBall.getBounds().y) {
                        // Bottom collision
                        collidedY = Direction.DOWN;
                    }
                }
            }
        }

        for (Entity ent : entities) {
            if (Intersector.overlaps(theBall.getBounds(), ent.getBounds())) {
                if (ent.getDamage() > 0) {
                    theBall.addDamage(ent.getDamage());
                    if (ent.getDamage() > 3)
                        Assets.hit2.play(0.8f);
                    else
                        Assets.hit.play(0.8f);
                    updateHealthBar();
                }

                if (ent instanceof Water) {
                    System.out.println("Water touched!");
                    Assets.jumpWater.play();
                    theBall.setWet();
                }
            }
        }

        boolean collided = false;

        if (collidedX != Direction.NONE) {
            if (theBall.getVelocity().x == 0) {
                theBall.getVelocity().x = Ball.MAX_VELOCITY;
            }
            if (collidedX == Direction.LEFT) {
                theBall.setMovingDirection(Direction.RIGHT);
            } else {
                theBall.setMovingDirection(Direction.LEFT);
            }

            theBall.getVelocity().scl(-1, 1);
            collided = true;
        }

        if (collidedY != Direction.NONE) {
            if (collidedY == Direction.UP) {
                theBall.getVelocity().set(theBall.getVelocity().x,
                        -Ball.MAX_VELOCITY);
                theBall.setMovingDirection(Direction.DOWN);
            } else {
                theBall.getVelocity().set(theBall.getVelocity().x,
                        Ball.MAX_VELOCITY);
                theBall.setMovingDirection(Direction.UP);
                if (theBall.isPlayer())
                    Assets.jump.play();
            }

            collided = true;
        }

        /*
         * if (ball.getPosition().y <= 0) {
         * ball.getVelocity().set(ball.getVelocity().x, Ball.MAX_VELOCITY);
         * ball.getPosition().scl(1, 0); }
         */

        if (theBall.isPlayer() && currentWorld < 4) {
            if (!collided) {
                if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                    theBall.getVelocity().set(-Ball.MAX_VELOCITY / 2,
                            theBall.getVelocity().y);
                    theBall.setMovingDirection(Direction.LEFT);
                } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                    theBall.getVelocity().set(Ball.MAX_VELOCITY / 2,
                            theBall.getVelocity().y);
                    theBall.setMovingDirection(Direction.RIGHT);
                } else if (Gdx.input.isKeyPressed(Keys.UP)) {
                    theBall.jump();
                }
            } else {
                theBall.setPosition(tmp);
            }

            if (theBall.isDead()) {
                if (lives > 1) {
                    lives -= 1;
                    startWorld(currentWorld, false);
                } else {
                    this.state = GameState.FINISHED;

                    displayInfo(Assets.gameover, "Press SPACE to restart");
                    lives = 0;
                    updateLifeBar();
                }
            }

            if (theBall.getBounds().x > mapWidth) {
                System.out.println("Got to the next world!");
                startWorld(currentWorld + 1, false);
            } else if (theBall.getBounds().x < 0) {
                System.out.println("Got to the previous world!");
                if (this.currentWorld == 1)
                    startWorld(1, false);
                else
                    startWorld(currentWorld - 1, false);
            }
        }

    }

    private void displayInfo(TextureRegion region, String text) {
        infoTable = new Table();
        infoTable.setFillParent(true);

        Image img = new Image(region);
        // img.setX(guiStage.getWidth() / 2 - img.getWidth() / 2);
        // img.setY(guiStage.getHeight() / 2 - img.getHeight() / 2);

        infoTable.add(img).expandY().row();

        Label label = new Label(text, Assets.skin);
        // label.setX(guiStage.getWidth() / 2 - label.getWidth() / 2);
        label.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(0.5f),
                Actions.fadeIn(0.5f))));

        infoTable.add(label);

        guiStage.addActor(infoTable);
        // guiStage.addActor(img);

    }

    private void hideInfo() {
        if (infoTable != null)
            infoTable.remove();
    }

    private void updateHealthBar() {
        healthBar.clearChildren();
        for (int i = 0; i < ball.getHealth() / 2; i++) {
            Image health = new Image(Assets.health);
            healthBar.add(health).pad(2f);
        }
        healthBar.pack();
        healthBar.setPosition(0, guiStage.getHeight() - healthBar.getHeight());
        guiStage.addActor(healthBar);
    }

    private void updateLifeBar() {
        lifeBar.clearChildren();
        for (int i = 0; i < lives; i++) {
            Image life = new Image(Assets.ball);
            lifeBar.add(life).pad(2f);
        }
        lifeBar.pack();
        lifeBar.setPosition(guiStage.getWidth() - lifeBar.getWidth(),
                guiStage.getHeight() - lifeBar.getHeight());
        guiStage.addActor(lifeBar);
    }

    private void addIndicators() {
        back = new Image(Assets.back);
        next = new Image(Assets.next);

        back.setPosition(10, guiStage.getHeight() / 2 - back.getHeight() / 2);
        next.setPosition(guiStage.getWidth() - next.getWidth() - 10,
                guiStage.getHeight() / 2 - back.getHeight() / 2);

        back.addAction(Actions.sequence(
                Actions.repeat(
                        5,
                        Actions.sequence(Actions.moveBy(10, 0, 0.5f),
                                Actions.moveBy(-10, 0, 0.5f))), Actions.hide()));
        next.addAction(Actions.sequence(
                Actions.repeat(
                        5,
                        Actions.sequence(Actions.moveBy(-10, 0, 0.5f),
                                Actions.moveBy(10, 0, 0.5f))), Actions.hide()));
        if (currentWorld > 1)
            guiStage.addActor(back);

        guiStage.addActor(next);

    }

    // Source:
    // http://gamedev.stackexchange.com/questions/74926/libgdx-keep-camera-within-bounds-of-tiledmap
    private void centerCamera() {
        camera.position.set(ball.getBounds().x, ball.getBounds().y, 0);

        float mapLeft = 0;
        float mapRight = 0 + mapWidth;
        float mapBottom = 0;
        float mapTop = 0 + mapHeight;

        float cameraHalfWidth = camera.viewportWidth * .5f;
        float cameraHalfHeight = camera.viewportHeight * .5f;

        float cameraLeft = camera.position.x - cameraHalfWidth;
        float cameraRight = camera.position.x + cameraHalfWidth;
        float cameraBottom = camera.position.y - cameraHalfHeight;
        float cameraTop = camera.position.y + cameraHalfHeight;

        if (cameraLeft <= mapLeft) {
            camera.position.x = mapLeft + cameraHalfWidth;
        } else if (cameraRight >= mapRight) {
            camera.position.x = mapRight - cameraHalfWidth;
        }

        if (cameraBottom <= mapBottom) {
            camera.position.y = mapBottom + cameraHalfHeight;
        } else if (cameraTop >= mapTop) {
            camera.position.y = mapTop - cameraHalfHeight;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        Assets.dispose();
    }

    private class InputHandler extends InputAdapter {
        @Override
        public boolean keyUp(int keycode) {
            switch (keycode) {

            case Keys.P:
                if (state == GameState.RUNNING) {
                    state = GameState.PAUSED;
                    displayInfo(Assets.paused, "Press P to continue");
                } else {
                    state = GameState.RUNNING;
                    hideInfo();
                }
                break;

            case Keys.SPACE:
                if (state == GameState.FINISHED || currentWorld == 4) {
                    lives = DEF_LIVES;
                    startWorld(1, true);
                }
                break;

            default:
                break;
            }
            return super.keyUp(keycode);
        }
    }
}
