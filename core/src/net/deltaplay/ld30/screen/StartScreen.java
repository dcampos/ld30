package net.deltaplay.ld30.screen;

import net.deltaplay.ld30.Assets;
import net.deltaplay.ld30.Ld30;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class StartScreen extends ScreenAdapter {
    private Stage guiStage;
    private Ld30 game;

    public StartScreen(Ld30 game) {
        this.game = game;

        Assets.loadAll();

        this.guiStage = new Stage(new ExtendViewport(
                (float) Gdx.graphics.getWidth(),
                (float) Gdx.graphics.getHeight()));

        Gdx.input.setInputProcessor(new InputMultiplexer(guiStage));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.682f, 0.886f, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        guiStage.act();
        guiStage.draw();

        super.render(delta);

        if (Gdx.input.isKeyPressed(Keys.SPACE)) {
            Assets.loop.stop();
            game.setScreen(new GameScreen(game));
        }
    }

    @Override
    public void show() {
        TiledDrawable bground = new TiledDrawable(Assets.bground);

        Table table = new Table();
        table.setFillParent(true);
        table.setBackground(bground);

        Image logo = new Image(Assets.logo);
        table.add(logo).expandY().row();

        Label label = new Label("Press SPACE to start", Assets.skin);
        label.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(0.5f),
                Actions.fadeIn(0.5f))));
        table.add(label);

        guiStage.addActor(table);

        Assets.loop.setLooping(true);
        Assets.loop.play();

        super.show();
    }
}
