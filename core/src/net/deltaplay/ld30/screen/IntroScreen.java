package net.deltaplay.ld30.screen;

import net.deltaplay.ld30.Assets;
import net.deltaplay.ld30.Ld30;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class IntroScreen extends ScreenAdapter {
    private Stage guiStage;
    private Ld30 game;

    public IntroScreen(Ld30 game) {
        this.game = game;

        Assets.loadAll();

        this.guiStage = new Stage(new ExtendViewport(
                (float) Gdx.graphics.getWidth(),
                (float) Gdx.graphics.getHeight()));

        Gdx.input.setInputProcessor(new InputMultiplexer(guiStage));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        guiStage.act();
        guiStage.draw();

        super.render(delta);
    }

    @Override
    public void show() {

        Image logo = new Image(Assets.deltaplay);
        logo.setPosition(guiStage.getWidth() / 2 - logo.getWidth() / 2,
                guiStage.getHeight() / 2 - logo.getHeight() / 2);
        logo.addAction(Actions.sequence(Actions.fadeOut(0), Actions.fadeIn(2f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        game.setScreen(new StartScreen(game));
                    }
                })));
        guiStage.addActor(logo);

        super.show();
    }
}
