package net.deltaplay.ld30;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Assets {
    public static TextureAtlas atlas;
    public static TextureRegion ball;
    public static TextureRegion ball2;
    public static TextureRegion ballDead;
    public static TextureRegion ballSad;
    public static TextureRegion ballUp;
    public static TextureRegion ballDown;
    public static TextureRegion ballRight;
    public static TextureRegion ballLeft;
    public static TextureRegion spikes;
    public static TextureRegion topSpikes;
    public static TextureRegion saw;
    public static TextureRegion health;

    public static TextureRegion logo;
    public static TextureRegion deltaplay;
    public static TextureRegion youwon;
    public static TextureRegion gameover;
    public static TextureRegion paused;
    public static TextureRegion bground;

    public static TextureRegion next;
    public static TextureRegion back;

    public static Sound jump;
    public static Sound jumpWater;
    public static Sound hit;
    public static Sound hit2;
    public static Sound win;

    public static Music loop;

    public static Skin skin;

    public static void loadAll() {
        atlas = new TextureAtlas(Gdx.files.internal("data/ld30.atlas"));
        ball = atlas.findRegion("ball");
        ball2 = atlas.findRegion("ball2");
        ballDead = atlas.findRegion("balldead");
        ballSad = atlas.findRegion("ballsad");
        ballUp = atlas.findRegion("ballup");
        ballDown = atlas.findRegion("balldown");
        ballRight = atlas.findRegion("ballright");
        ballLeft = atlas.findRegion("ballleft");
        spikes = atlas.findRegion("spikes");
        topSpikes = new TextureRegion(spikes);
        topSpikes.flip(false, true);
        saw = atlas.findRegion("saw");
        health = atlas.findRegion("health");

        logo = atlas.findRegion("logo");
        deltaplay = atlas.findRegion("deltaplay");
        bground = atlas.findRegion("rock");
        youwon = atlas.findRegion("youwon");
        gameover = atlas.findRegion("gameover");
        paused = atlas.findRegion("paused");

        next = atlas.findRegion("next");
        back = atlas.findRegion("back");

        jump = Gdx.audio.newSound(Gdx.files.internal("data/jump2.wav"));
        jumpWater = Gdx.audio
                .newSound(Gdx.files.internal("data/jumpwater.wav"));

        hit = Gdx.audio.newSound(Gdx.files.internal("data/hit.wav"));
        hit2 = Gdx.audio.newSound(Gdx.files.internal("data/hit2.wav"));
        win = Gdx.audio.newSound(Gdx.files.internal("data/win.wav"));

        loop = Gdx.audio.newMusic(Gdx.files.internal("data/loop.ogg"));

        skin = new Skin(Gdx.files.internal("data/skin.json"));
    }

    public static void dispose() {
        atlas.dispose();
    }
}
