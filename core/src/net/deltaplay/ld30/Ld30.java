package net.deltaplay.ld30;

import net.deltaplay.ld30.screen.IntroScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Ld30 extends Game {
    private FPSLogger fps;
    private SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        fps = new FPSLogger();
        this.setScreen(new IntroScreen(this));
    }

    @Override
    public void render() {
        super.render();
        fps.log();
    }

    public SpriteBatch getBatch() {
        return batch;
    }
}
