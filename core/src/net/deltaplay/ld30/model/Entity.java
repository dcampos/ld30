package net.deltaplay.ld30.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity {
    Vector2 position = new Vector2();
    Rectangle bounds;
    float damage;

    public float getDamage() {
        return damage;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public abstract void update(float delta);
}
