package net.deltaplay.ld30.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class Ball {
    public static final float MAX_VELOCITY = 320f;
    public static final float DEF_IMMUNITY = 1f;
    public static final float DEF_DRYING = 10f;

    private enum State {
        ALIVE, INJURED, DEAD
    }

    Vector2 position = new Vector2();
    Vector2 acceleration = new Vector2();
    Vector2 velocity = new Vector2();

    Circle bounds;

    private Direction movingDirection;
    private boolean jumped;

    private float health;
    private float immunityTime;
    private State state;

    private boolean isPlayer;
    private float dryingTime;

    public Ball(Vector2 pos, boolean player) {
        this.position = pos;
        this.acceleration.set(0, -MAX_VELOCITY);
        this.velocity.set(0, -MAX_VELOCITY);
        this.movingDirection = Direction.NONE;
        this.bounds = new Circle(pos.cpy().add(16f, 16f), 16f);
        this.health = 6f;
        this.immunityTime = 0;
        this.state = State.ALIVE;
        this.isPlayer = player;

        this.dryingTime = 0f;
    }

    public void update(float delta) {
        if (velocity.x < 0 && movingDirection == Direction.LEFT) {
            acceleration.set(MAX_VELOCITY / 2, acceleration.y);
        } else if (velocity.x > 0 && movingDirection == Direction.RIGHT) {
            acceleration.set(-MAX_VELOCITY / 2, acceleration.y);
        } else {
            acceleration.set(0, acceleration.y);
            velocity.x = 0;
            movingDirection = Direction.NONE;
        }

        velocity.add(acceleration.cpy().scl(delta));

        if (Math.abs(velocity.y) > MAX_VELOCITY) {
            velocity.y = (velocity.y > 0 ? MAX_VELOCITY : -MAX_VELOCITY);
        }

        position.add(velocity.cpy().scl(isWet() ? delta / 3 : delta));
        bounds.setPosition(position.cpy().add(16f, 16f));

        if (immunityTime != 0) {
            immunityTime = immunityTime < 0 ? 0 : immunityTime - delta;
        }

        if (dryingTime != 0) {
            dryingTime = dryingTime < 0 ? 0 : dryingTime - delta;
        }
    }

    public void addDamage(float damage) {
        if (immunityTime == 0) {
            this.health -= damage;
            this.immunityTime = DEF_IMMUNITY;
            Gdx.app.log("BALL", "* Damage suffered: " + damage + ". Health: "
                    + this.health);
            checkHealth();
        }
    }

    private void checkHealth() {
        if (this.health <= 0) {
            this.state = State.DEAD;
            Gdx.app.log("BALL", "* The ball is dead!");
        }
    }

    public boolean isDead() {
        return this.state == State.DEAD;
    }

    public boolean isInjured() {
        return this.immunityTime > 0;
    }

    public boolean isWet() {
        return this.dryingTime > 0;
    }

    public void setWet() {
        this.dryingTime = DEF_DRYING;
    }

    public boolean canJump() {
        return (!jumped && velocity.y > 0);
    }

    public void jump() {
        if (canJump()) {
            this.jumped = true;
            this.getVelocity().set(velocity.x, MAX_VELOCITY);
        }
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 pos) {
        position.set(pos);
        bounds.setPosition(position.cpy().add(16f, 16f));
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public Direction getMovingDirection() {
        return movingDirection;
    }

    public Circle getBounds() {
        return bounds;
    }

    public float getHealth() {
        return health;
    }

    public void setMovingDirection(Direction movingDirection) {
        this.movingDirection = movingDirection;

        if (this.movingDirection == Direction.UP) {
            this.jumped = false;
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isPlayer() {
        return isPlayer;
    }
}
