package net.deltaplay.ld30.model;

import net.deltaplay.ld30.screen.GameScreen;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Spike extends Entity {
    boolean top;

    public Spike(boolean top, Vector2 vector2) {
        this.top = top;
        this.position = vector2;
        this.bounds = new Rectangle(position.x, position.y,
                GameScreen.TILE_WIDTH, GameScreen.TILE_WIDTH);
        this.damage = 2f;
    }

    public boolean isTop() {
        return top;
    }

    @Override
    public void update(float delta) {
    }

}
