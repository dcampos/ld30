package net.deltaplay.ld30.model;

import net.deltaplay.ld30.screen.GameScreen;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class FlyingSaw extends Entity {
    Vector2 velocity = new Vector2();
    private Direction movingDirection;

    public FlyingSaw(Vector2 pos) {
        this.velocity = new Vector2(-180, 0);
        this.position = pos;
        this.bounds = new Rectangle(position.x, position.y,
                GameScreen.TILE_WIDTH, GameScreen.TILE_WIDTH);
        this.damage = 6f;
        this.movingDirection = Direction.LEFT;
    }

    @Override
    public void update(float delta) {
        position.add(velocity.cpy().scl(delta));
        bounds.setPosition(position.cpy());
    }

    public Direction getMovingDirection() {
        return movingDirection;
    }

    public void setMovingDirection(Direction movingDirection) {
        this.movingDirection = movingDirection;
    }

    public void changeMovingDirection() {
        if (this.movingDirection == Direction.LEFT)
            this.movingDirection = Direction.RIGHT;
        else
            this.movingDirection = Direction.LEFT;

        this.velocity.scl(-1, 0);
    }
}
