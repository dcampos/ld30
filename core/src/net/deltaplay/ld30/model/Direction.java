package net.deltaplay.ld30.model;

public enum Direction {
    RIGHT, LEFT, UP, DOWN, NONE
}