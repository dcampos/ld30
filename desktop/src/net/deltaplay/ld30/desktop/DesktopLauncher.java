package net.deltaplay.ld30.desktop;

import net.deltaplay.ld30.Ld30;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "PitBall by DeltaPlay.net";
        new LwjglApplication(new Ld30(), config);
    }
}
